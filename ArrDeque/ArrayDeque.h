#pragma once

#include <iostream>

class ArrayDeque {
private:
	static const int defaultExpansionFactor = 2;
	static const int initialCapacity = 10;

public:
	ArrayDeque(size_t expansionFactor = defaultExpansionFactor, bool shrinking = true);
	ArrayDeque(const ArrayDeque& other);
	ArrayDeque& operator=(const ArrayDeque& other);
	~ArrayDeque();

	void push_back(double value);
	void push_front(double value);
	double pop_back();
	double pop_front();
	double back();
	double front();

	size_t size() const;
	size_t getCapacity() const;
	bool isEmpty() const;
	size_t getExpansionFactor() const;
	bool isShrinking() const;

	void setExpansionFactor(size_t expansionFactor);
	void setShrinking(bool shrinking = true);

	void clear();
	void print(std::ostream& out = std::cout) const;
private:
	double* data;
	size_t capacity;

	size_t leftIndex;
	size_t rightIndex;

	size_t expansionFactor;
	bool shrinking;

	void resize(size_t newCapacity);
	void shrink();
	void expand();

	void copyData(double* data, size_t leftIndex, size_t rightIndex, size_t capacity);
	void clearData(bool revertToInitial = false);
};

std::ostream& operator<<(std::ostream& out, const ArrayDeque& deque);