// ArrDeque.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include "ArrayDeque.h"

int main() {
    ArrayDeque deque;

    std::cout << deque;
    deque.push_back(5);
    deque.push_back(6);
    deque.push_back(7);
    deque.push_back(5);
    deque.push_back(6);
    deque.push_back(7);
    deque.push_back(5);
    deque.push_back(6);
    deque.push_back(7);


    std::cout << deque;

    for (int i = 0; i < 20; ++i) {
        deque.push_front(i);
    }

    ArrayDeque deque2(deque);

    for (int i = 0; i < 20; ++i) {
        deque.pop_back();
    }

    std::cout << deque2.size();

    deque2.push_front(1);
    std::cout << std::endl;

    std::cout << deque2.size();
    

    return 0;
}