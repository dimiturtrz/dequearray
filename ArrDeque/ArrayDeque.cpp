#include "ArrayDeque.h"

// --------------------------------- BIG FOUR ------------------------------------
ArrayDeque::ArrayDeque(size_t expansionFactor, bool shrinking): expansionFactor(expansionFactor), shrinking(shrinking) {
	capacity = initialCapacity;
	data = new double[capacity];
	leftIndex = rightIndex = capacity / 2;
}

ArrayDeque::ArrayDeque(const ArrayDeque& other): data(nullptr) {
	copyData(other.data, other.leftIndex, other.rightIndex, other.capacity);
}

ArrayDeque& ArrayDeque::operator=(const ArrayDeque& other) {
	if (this == &other) {
		return *this;
	}

	copyData(other.data, other.leftIndex, other.rightIndex, other.capacity);
	return *this;
}

ArrayDeque::~ArrayDeque() {
	clearData();
}

// --------------------------------- SIZE HANDLERS ------------------------------------
void ArrayDeque::resize(size_t newCapacity) {
	int size = this->size();
	int newStart = newCapacity / 2 - size / 2;
	double* newData = new double[newCapacity];
	for (int i = 0; i < size; ++i) {
		newData[newStart + i] = data[leftIndex + i];
	}
	delete[] data;
	data = newData;
	capacity = newCapacity;
	leftIndex = newStart;
	rightIndex = newStart + size;
}

void ArrayDeque::shrink() {
	if (capacity < initialCapacity) {
		return;
	}

	resize(capacity / expansionFactor);
}

void ArrayDeque::expand() {
	resize(capacity * expansionFactor);
}

// --------------------------------- UTILITY ------------------------------------
void ArrayDeque::copyData(double* data, size_t leftIndex, size_t rightIndex, size_t capacity) {
	clearData();

	this->rightIndex = rightIndex;
	this->leftIndex = leftIndex;
	this->capacity = capacity;
	this->data = new double[capacity];
	for (size_t i = leftIndex; i < rightIndex; ++i) {
		this->data[i] = data[i];
	}
}

void ArrayDeque::clearData(bool revertToInitial) {
	delete[] data;
	data = (revertToInitial ? new double[initialCapacity] : nullptr);
	capacity = (revertToInitial ? initialCapacity : 0);
	leftIndex = rightIndex = capacity / 2;
}

// --------------------------------- CONTAINER OPERATIONS ------------------------------------
void ArrayDeque::push_back(double value) {
	if (rightIndex >= capacity) {
		expand();
	}

	data[rightIndex++] = value;
}

void ArrayDeque::push_front(double value) {
	if (leftIndex - 1 <= 0) {
		expand();
	}

	data[--leftIndex] = value;
}

double ArrayDeque::pop_back() {
	double deletedValue = data[rightIndex--];
	if (size()*expansionFactor < capacity && shrinking) {
		shrink();
	}

	return deletedValue;
}

double ArrayDeque::pop_front() {
	double deletedValue = data[leftIndex++];
	if (size() * expansionFactor < capacity && shrinking) {
		shrink();
	}

	return deletedValue;
}

double ArrayDeque::back() {
	return data[rightIndex-1];
}

double ArrayDeque::front() {
	return data[leftIndex];
}


// --------------------------------- GETTERS ------------------------------------

size_t ArrayDeque::size() const {
	return rightIndex - leftIndex;
}

size_t ArrayDeque::getCapacity() const {
	return capacity;
}

bool ArrayDeque::isEmpty() const {
	//return size() == 0
	return leftIndex == rightIndex;
}

size_t ArrayDeque::getExpansionFactor() const {
	return expansionFactor;
}

bool ArrayDeque::isShrinking() const {
	return shrinking;
}

// --------------------------------- SETTERS ------------------------------------

void ArrayDeque::setExpansionFactor(size_t expansionFactor) {
	this->expansionFactor = expansionFactor;
}

void ArrayDeque::setShrinking(bool shrinking) {
	this->shrinking = shrinking;
}

void ArrayDeque::clear() {
	clearData(true);
}

// --------------------------------- OUTPUT ------------------------------------

void ArrayDeque::print(std::ostream& out) const {
	for (size_t i = leftIndex; i < rightIndex; ++i) {
		out << data[i] << " ";
	}
	out << std::endl;
}

std::ostream& operator<<(std::ostream& out, const ArrayDeque& deque) {
	deque.print(out);
	return out;
}
