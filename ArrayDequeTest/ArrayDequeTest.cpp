#include "pch.h"
#include "CppUnitTest.h"
#include "../ArrDeque/ArrayDeque.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace ArrDequeTests {

	TEST_CLASS(ArrayDequeBigFourSizeTests) {
	public:
		ArrayDeque testDeque;
		TEST_METHOD_INITIALIZE(SetupTestDeques) {
			for (int i = 1; i < 4; ++i) {
				testDeque.push_back(i);
				testDeque.push_front(i);
			}
		}

		TEST_METHOD_CLEANUP(CleanTestDeques) {
			testDeque.clear();
		}

		TEST_METHOD(ArrayDequeConstructorSizeTest) {
			ArrayDeque deque;
			Assert::AreEqual(size_t(0), deque.size());
		}

		TEST_METHOD(ArrayDequeCopyConstructorSizeTest) {
			ArrayDeque deque(testDeque);
			Assert::AreEqual(testDeque.size(), deque.size());
		}

		TEST_METHOD(ArrayDequeAssignmentOperatorSizeTest) {
			ArrayDeque deque;
			deque = testDeque;
			Assert::AreEqual(testDeque.size(), deque.size());
		}
	};

	TEST_CLASS(ArrayDequeAcessorsMutatorsTests) {
	public:
		ArrayDeque testDeque;
		TEST_METHOD_INITIALIZE(SetupTestDeques) {
			for (int i = 0; i < 10; ++i) {
				testDeque.push_back(i);
			}
		}

		TEST_METHOD(ArrayDequeSizeGetterTest) {
			Assert::AreEqual(size_t(10), testDeque.size());
		}

		TEST_METHOD(ArrayDequeCapacityGetterTest) {
			Assert::AreEqual(size_t(20), testDeque.getCapacity());
		}

		TEST_METHOD(ArrayDequeIsntEmptyTest) {
			Assert::IsFalse(testDeque.isEmpty());
		}

		TEST_METHOD(ArrayDequeIsEmptyTest) {
			testDeque.clear();
			Assert::IsTrue(testDeque.isEmpty());
		}

		TEST_METHOD(ArrayDequeExpansionFactorDefaultValueTest) {
			Assert::AreEqual(size_t(2), testDeque.getExpansionFactor());
		}

		TEST_METHOD(ArrayDequeExpansionFactorValueTest) {
			testDeque.setExpansionFactor(3);
			Assert::AreEqual(size_t(3), testDeque.getExpansionFactor());
		}

		TEST_METHOD(ArrayDequeShrinkingDefaultValueTest) {
			Assert::IsTrue(testDeque.isShrinking());
		}

		TEST_METHOD(ArrayDequeShrinkingValueTest) {
			testDeque.setShrinking(false);
			Assert::IsFalse(testDeque.isShrinking());
		}
	};

	TEST_CLASS(ArrayDequeOperations) {
	public:
		ArrayDeque testDeque;
		TEST_METHOD_INITIALIZE(SetupTestDeques) {
			for (int i = 0; i < 10; ++i) {
				testDeque.push_back(i);
			}
		}

		TEST_METHOD_CLEANUP(CleanTestDeques) {
			testDeque.clear();
		}

		TEST_METHOD(ArrayDequePushFrontSizeTest) {
			testDeque.push_front(1);
			Assert::AreEqual(size_t(11), testDeque.size());
		}

		TEST_METHOD(ArrayDequePushBackSizeTest) {
			testDeque.push_back(1);
			Assert::AreEqual(size_t(11), testDeque.size());
		}

		TEST_METHOD(ArrayDequePushFrontValueTest) {
			testDeque.push_front(1);
			Assert::AreEqual(1., testDeque.front());
		}

		TEST_METHOD(ArrayDequePushBackValueTest) {
			testDeque.push_back(1);
			Assert::AreEqual(1., testDeque.back());
		}

		TEST_METHOD(ArrayDequePushFrontPreviousValueTest) {
			double frontValue = testDeque.front();
			testDeque.push_front(1);
			testDeque.pop_front();
			Assert::AreEqual(frontValue, testDeque.front());
		}

		TEST_METHOD(ArrayDequePushBackPreviousValueTest) {
			double frontValue = testDeque.back();
			testDeque.push_back(1);
			testDeque.pop_back();
			Assert::AreEqual(frontValue, testDeque.back());
		}
	};


	/*TEST_CLASS(ArrayDequeOperations) {
	public:
		ArrayDeque testDeque1;
		ArrayDeque testDeque2;
		TEST_METHOD_INITIALIZE(SetupTestDeques) {
			for (int i = 1; i < 4; ++i) {
				testDeque1.push_back(i);
				testDeque1.push_front(i);
			}

			for (int i = 0; i < 9; ++i) {
				testDeque2.push_back(i);
			}
		}

		TEST_METHOD_CLEANUP(CleanTestDeques) {
			testDeque.clear();
		}

		TEST_METHOD(ArrayDequeConstructor) {
			ArrayDeque deque;
			Assert::AreEqual(size_t(0), deque.size());
		}
	};*/
}
